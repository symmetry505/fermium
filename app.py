#!/usr/bin/env python
# -*-coding: utf-8 -*-

from flask import Flask, render_template

from utils import info

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html', **info)


if __name__ == "__main__":
    app.run(debug=True)
